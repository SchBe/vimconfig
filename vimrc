syntax on
set number
color darkblue

map <C-n> :NERDTree<CR>
let NERDTreeMinimalUI=1
let g:NERDTreeNodeDelimiter = "\u00a0"

let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "M",
    \ "Staged"    : "+",
    \ "Untracked" : "-",
    \ "Renamed"   : "R",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "X",
    \ "Dirty"     : "D",
    \ "Clean"     : "C",
    \ 'Ignored'   : 'I',
    \ "Unknown"   : "?"
    \ }

let g:ale_linters = {'javascript': ['eslint']}
let g:ale_fixers = {'javascript': ['eslint']}
let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}
let g:ale_linters = {'vue': ['eslint']}
let g:ale_sign_column_always = 1

highlight GitGutterAdd guifg=#009900

set tabstop=4
set shiftwidth=4
set smartindent

set noexpandtab
set copyindent
set softtabstop=0

set backupcopy=yes
